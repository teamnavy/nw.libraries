﻿USE [ResourceDB]
GO

--CREATE CLUSTERED INDEX IX_CMS_Languages_LanguageId ON [CMS_Languages] (LanguageId);

truncate table [CMS_Languages]
go
insert into [dbo].[CMS_Languages] VALUES(1,'EN', 'en-GB', 'EN')
insert into [dbo].[CMS_Languages] VALUES(9,'TR', 'tr-TR', 'TR')
GO


--CREATE CLUSTERED INDEX IX_CMS_Resource_ResourceId ON [CMS_Resource] (ResourceId);
 
truncate table CMS_Resource
go
insert into  [CMS_Resource] VALUES(1,	NULL,	'Member',	1,	'en-GB',	'Register',	'OPEN A NEW ACCOUNT')
insert into  [CMS_Resource] VALUES(1,	NULL,	'Member',	9,	'tr-TR',	'Register',	'YENİ HESAP OLUŞTUR')

GO
SELECT * FROM CMS_Resource



--CREATE CLUSTERED INDEX IX_CMS_Domains_DomainId ON [CMS_Domains] (DomainId);

set identity_insert dbo.CMS_Domains on
insert into [dbo].[CMS_Domains] (DomainId,Domain) VALUES(1,'Casino')
set identity_insert dbo.CMS_Domains off

go



