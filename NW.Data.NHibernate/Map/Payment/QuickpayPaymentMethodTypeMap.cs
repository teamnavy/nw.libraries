﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NW.Data.NHibernate.Map.Payment
{
    public class QuickpayPaymentMethodTypeMap : ClassMap<NW.Core.Entities.Payment.QuickpayPaymentMethodType>
    {
        public QuickpayPaymentMethodTypeMap()
        {
            Id(x => x.Id);
            Map(x => x.Name);
            Map(x => x.VoltronProviderId);


            //HasMany(x => x.MemberDetails).KeyColumn("MemberId").Inverse().Cascade.All();
            Table("QuickpayPaymentMethodType");
            //Bag(x => x.MemberDetails, colmap =>  { colmap.Key(x => x.Column("MemberId")); colmap.Inverse(true); }, map => { map.OneToMany(); });
        }
    }
}
