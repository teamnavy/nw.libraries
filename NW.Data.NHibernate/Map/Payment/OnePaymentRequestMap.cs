﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NW.Data.NHibernate.Map.Payment
{
    public class OnePaymentRequestMap : ClassMap<NW.Core.Entities.Payment.OnePaymentRequest>
    {
        public OnePaymentRequestMap()
        {
            Id(x => x.Id);
            Map(x => x.MemberId);
            Map(x => x.StatusType);
            Map(x => x.Amount);
            Map(x => x.CreateDate);
            Map(x => x.UpdateDate);
            Map(x => x.RecognisedAmount);
            Map(x => x.ProviderUniqueId);
            Map(x => x.PaymentProviderId);
            Map(x => x.PaymentMethodType);
            Map(x => x.RequestData);
            Map(x => x.CallbackData);

            Table("OnePaymentRequest");
        }
    }
}
