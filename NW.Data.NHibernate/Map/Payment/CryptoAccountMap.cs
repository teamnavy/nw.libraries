﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NW.Data.NHibernate.Map.Payment
{
    public class CryptoAccountMap : ClassMap<NW.Core.Entities.Payment.CryptoAccount>
    {
        public CryptoAccountMap()
        {
            Id(x => x.Id);
            Map(x => x.NetworkId);
            Map(x => x.MemberId);
            Map(x => x.StatusType);
            Map(x => x.Address);
            Map(x => x.CreateDate);

            Table("CryptoAccount");
        }
    }
}
