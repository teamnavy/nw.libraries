﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NW.Data.NHibernate.Map.Game
{
    public class CasinoProviderMap : ClassMap<NW.Core.Entities.CasinoProvider>
    {

        public CasinoProviderMap()
        {
            Id(x => x.Id);
            Map(x => x.Name);
            Map(x => x.StatusType);
            Map(x => x.CreateDate);
            Map(x => x.OrderNumber);
            Map(x => x.LogoPath);
            Map(x => x.VoltronProviderId);
            Map(x => x.SystemName);
            Map(x => x.IsLive);


            Table("CasinoProvider");
        }
    }
}