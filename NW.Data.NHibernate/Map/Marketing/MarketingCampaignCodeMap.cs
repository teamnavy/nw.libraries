using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using NW.Core.Entities;
using FluentNHibernate.Mapping;
using NW.Core.Entities.Marketing;

namespace NW.Data.NHibernate.Maps{
    
    
    public class MarketingCampaignCodeMap : ClassMap<MarketingCampaignCode> {

        public MarketingCampaignCodeMap()
        {
            Table("MarketingCampaignCode");

            // If you're using string ID, ensure it's set to Assigned or something suitable
            // e.g., if the code is user-specified:
            Id(x => x.Id);

            Map(x => x.Code);
            Map(x => x.StatusType);
            Map(x => x.CreateDate);
            Map(x => x.UpdateDate);
            Map(x => x.UseCount);
            Map(x => x.UsedCount);


            References(x => x.MarketingCampaign).Column("MarketingCampaignId").Not.Nullable();

        }
    }
}

