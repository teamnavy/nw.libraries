﻿using FluentNHibernate.Mapping;
using NW.Core.Entities.Marketing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NW.Data.NHibernate.Map.Marketing
{
    public class LuckWheelEligibleEmailMap : ClassMap<LuckWheelEligibleEmail>
    {

        public LuckWheelEligibleEmailMap()
        {
            Id(x => x.Id);
            Map(x => x.Email);
            Map(x => x.StatusType);
            Map(x => x.CreateDate);
            Map(x => x.UpdateDate);
            Map(x => x.AwardCode);

            //HasMany(x => x.MemberDetails).KeyColumn("MemberId").Inverse().Cascade.All();
            Table("LuckWheelEligibleEmail");
            //Bag(x => x.MemberDetails, colmap =>  { colmap.Key(x => x.Column("MemberId")); colmap.Inverse(true); }, map => { map.OneToMany(); });
        }
    }
}
