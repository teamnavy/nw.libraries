using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using NW.Core.Entities;
using FluentNHibernate.Mapping;
using NW.Core.Entities.Marketing;

namespace NW.Data.NHibernate.Maps{
    
    
    public class MarketingCampaignMap : ClassMap<MarketingCampaign> {

        public MarketingCampaignMap()
        {
            Table("MarketingCampaign");

            Id(x => x.Id);
            Map(x => x.Name);
            Map(x => x.StatusType);
            Map(x => x.CreateDate);
            Map(x => x.UpdateDate);
            Map(x => x.ExpiryDate);
            Map(x => x.AffiliateId);
            Map(x => x.BonusId);
            Map(x => x.AwardType);
            Map(x => x.Award);
            Map(x => x.CodeCount);
            Map(x => x.CodeUsagePerMember);
            Map(x => x.IsLastTransactionDeposit);

            // If you want a one-to-many between MarketingCampaign and MarketingCampaignCode:
            HasMany(x => x.MarketingCampaignCodeList)
                .KeyColumn("MarketingCampaignId")    // FK column in MarketingCampaignCode table
                .Inverse()                           // child's side is the owner of the relationship
                .Cascade.All();          // or .Cascade.All() / .None() depending on your use case
        }
    }
}

