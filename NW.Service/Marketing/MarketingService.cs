﻿using NHibernate;
using NW.Core.Entities.Marketing;
using NW.Core.Enum;
using NW.Core.Repositories;
using NW.Core.Services.Marketing;
using NW.Core.Work;
using NW.Helper.Optimove;
using NW.Service.Payment.EcoPayz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static NHibernate.Engine.Query.CallableParser;

namespace NW.Service.Marketing
{
    public class MarketingService : BaseService, IMarketingService
    {
        IRepository<MarketingCampaignCodeHistory, int> MarketingCampaignCodeHistoryRespository { get; set; }
        IRepository<MarketingCampaignCode, int> MarketingCampaignCodeRespository { get; set; }
        IRepository<OptimoveTemplate, int> OptimoveTemplateRespository { get; set; }
        IRepository<LuckWheelEligibleEmail, int> LuckWheelEligibleEmailRespository { get; set; }
        public MarketingService(IRepository<MarketingCampaignCode, int> _marketingCampaignCodeRespository, IRepository<MarketingCampaignCodeHistory, int> _marketingCampaignCodeHistoryRespository, IRepository<OptimoveTemplate, int> _optimoveTemplateRespository, IRepository<LuckWheelEligibleEmail, int> _luckWheelEligibleEmailRespository, IUnitOfWork _unitOfWork, ISession _session)
            : base(_unitOfWork, _session)
        {
            OptimoveTemplateRespository = _optimoveTemplateRespository;
            LuckWheelEligibleEmailRespository = _luckWheelEligibleEmailRespository;
            MarketingCampaignCodeRespository = _marketingCampaignCodeRespository;
            MarketingCampaignCodeHistoryRespository = _marketingCampaignCodeHistoryRespository;
        }
        public IList<Core.Entities.Marketing.OptimoveTemplate> GetOptimoveTemplateList(int? templateType)
        {
            IQueryable<OptimoveTemplate> query = OptimoveTemplateRespository.GetAll().Where(ot => ot.StatusType == (int)StatusType.Active);
            if (templateType.HasValue)
                query = query.Where(ot => ot.TemplateType == templateType.Value);


            return query.OrderByDescending(ot => ot.CreateDate).ToList();
        }
        private int GetOptimoveChannelId(TemplateType templateType)
        {
            switch (templateType)
            {
                case TemplateType.Email:
                    return OptimoveHelper.EMAIL_CHANNEL;
                case TemplateType.SMS:
                    return OptimoveHelper.SMS_CHANNEL;
                default:
                    return -1;
            }
        }
        public void InsertOptimoveTemplate(Core.Enum.TemplateType templateType, Core.Enum.StatusType statusType, string name, string content)
        {
            using (ITransaction transaction = UnitOfWork.Current.BeginTransaction(Session))
            {
                OptimoveTemplate optimoveTemplate = OptimoveTemplateRespository.Insert(new OptimoveTemplate() { Name = name, TemplateType = (int)templateType, CreateDate = DateTime.UtcNow, StatusType = (int)statusType, Content = content });
                bool result = OptimoveHelper.AddTemplate(GetOptimoveChannelId(templateType), optimoveTemplate.Id, optimoveTemplate.Name);
                if (result)
                    transaction.Commit();
                else
                    transaction.Rollback();
            }
        }

        public void DeleteOptimoveTemplate(int id)
        {
            using (ITransaction transaction = UnitOfWork.Current.BeginTransaction(Session))
            {
                OptimoveTemplate optimoveTemplate = OptimoveTemplateRespository.Get(id);
                optimoveTemplate.StatusType = (int)StatusType.Deleted;
                optimoveTemplate.UpdateDate = DateTime.UtcNow;
                OptimoveTemplateRespository.Update(optimoveTemplate);

                bool result = OptimoveHelper.DeleteTemplate(GetOptimoveChannelId((TemplateType)optimoveTemplate.TemplateType), optimoveTemplate.Id);
                if (result)
                    transaction.Commit();
                else
                    transaction.Rollback();
            }
        }
        public LuckWheelEligibleEmail InsertLuckyWheelEligibleEmail(string email, string awardCode)
        {
            LuckWheelEligibleEmail result = null;
            using (ITransaction transaction = UnitOfWork.Current.BeginTransaction(Session))
            {
                result = LuckWheelEligibleEmailRespository.Insert(new LuckWheelEligibleEmail() { Email = email, StatusType = (int)StatusType.Passive, CreateDate = DateTime.UtcNow, UpdateDate = DateTime.UtcNow, AwardCode = awardCode });
                transaction.Commit();
            }
            return result;
        }

        public MarketingCampaign GetMatketingCampaignByCode(string code, int memberId)
        {
            MarketingCampaignCode marketingCampaignCode = MarketingCampaignCodeRespository.GetAll().FirstOrDefault(c => c.Code == code);
            if (marketingCampaignCode == null)
                return null;

            if (marketingCampaignCode.UsedCount < marketingCampaignCode.UseCount && marketingCampaignCode.StatusType == (int)StatusType.Active && marketingCampaignCode.MarketingCampaign.ExpiryDate > DateTime.UtcNow && marketingCampaignCode.MarketingCampaign.StatusType == (int)StatusType.Active && !MarketingCampaignCodeHistoryRespository.GetAll().Any(ch => ch.MarketingCampaignCodeId == marketingCampaignCode.Id && ch.MemberId == memberId))
                return marketingCampaignCode.MarketingCampaign;
            else
                return null;
        }
        public void IncreaseUsageMatketingCampaignByCode(string code, int memberId)
        {
            MarketingCampaignCode marketingCampaignCode = MarketingCampaignCodeRespository.GetAll().FirstOrDefault(c => c.Code == code);
            if (marketingCampaignCode != null)
            {
                using (ITransaction transaction = UnitOfWork.Current.BeginTransaction(Session))
                {
                    marketingCampaignCode.UsedCount++;
                    MarketingCampaignCodeRespository.Update(marketingCampaignCode);

                    MarketingCampaignCodeHistoryRespository.Insert(new MarketingCampaignCodeHistory() { CreateDate = DateTime.UtcNow, MarketingCampaignCodeId = marketingCampaignCode.Id, MemberId = memberId });


                    transaction.Commit();
                }
                
            }
        }

    }
}
