﻿using DbLocalization;
using Logging.Converters;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NHibernate;
using NW.Core.Entities;
using NW.Core.Repositories;
using NW.Core.Services;
using NW.Core.Work;
using NW.Helper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NW.Service
{
    public class MemberSegmentService : BaseService, IMemberSegmentService
    {
        private IMemberService MemberService { get; set; }
        private ITagService TagService { get; set; }
        private IRepository<MemberSegment, int> MemberSegmentRepository { get; set; }
        private IRepository<MemberSegmentMember, int> MemberSegmentMemberRepository { get; set; }
        private IRepository<MemberSegmentCronRunHistory, int> MemberSegmentCronRunHistoryRepository { get; set; }

        public MemberSegmentService(
            IMemberService _memberService,
            ITagService _tagService,
            IRepository<MemberSegment, int> _memberSegmentRepository,
            IRepository<MemberSegmentMember, int> _memberSegmentMemberRepository,
            IRepository<MemberSegmentCronRunHistory, int> _memberSegmentCronRunHistoryRepository,
            IUnitOfWork _unitOfWork, ISession _session)
            : base(_unitOfWork, _session)
        {
            MemberService = _memberService;
            TagService = _tagService;
            MemberSegmentRepository = _memberSegmentRepository;
            MemberSegmentMemberRepository = _memberSegmentMemberRepository;
            MemberSegmentCronRunHistoryRepository = _memberSegmentCronRunHistoryRepository;
        }

        public MemberSegment MemberSegment(int id)
        {
            return MemberSegmentRepository.Get(id);
        }
        public IList<MemberSegment> GetAllActiveMemberSegments()
        {
            return MemberSegmentRepository.GetAll().Where(ms => ms.StatusType == 1).ToList();
        }
        public PagingModel<MemberSegment> GetMemberSegments(int pageIndex, int pageSize)
        {

            PagingModel<MemberSegment> pagingModel = new PagingModel<MemberSegment>();
            using (var unitOfWork = UnitOfWork.Current)
            {
                List<Transaction> result = new List<Transaction>();
                using (ITransaction transaction = unitOfWork.BeginTransaction(Session))
                {
                    pagingModel.TotalCount = MemberSegmentRepository.GetAll().Count();
                    pagingModel.ItemList = Session.QueryOver<MemberSegment>().Where(ms => ms.StatusType == 1 || ms.StatusType == 0)
                            .OrderBy(t => t.CreateDate).Desc
                            .Skip(pageIndex * pageSize)
                            .Take(pageSize)
                            .List();
                }
            }
            return pagingModel;
        }
        public MemberSegment InsertMemberSegment(MemberSegment memberSegment)
        {
            using (var unitOfWork = UnitOfWork.Current)
            {
                using (ITransaction transaction = unitOfWork.BeginTransaction(Session))
                {
                    memberSegment.CreateDate = DateTime.Now;
                    memberSegment.UpdateDate = DateTime.Now;
                    memberSegment = MemberSegmentRepository.Insert(memberSegment);
                    unitOfWork.Commit(transaction);
                    return memberSegment;
                }
            }
        }
        public void UpdateMemberSegment(MemberSegment memberSegment)
        {
            using (var unitOfWork = UnitOfWork.Current)
            {
                using (ITransaction transaction = unitOfWork.BeginTransaction(Session))
                {
                    memberSegment.UpdateDate = DateTime.Now;
                    memberSegment = MemberSegmentRepository.Update(memberSegment);
                    unitOfWork.Commit(transaction);
                }
            }
        }
        //public IList<MemberSegment> MemberSegmentsForMemberTagFilter(MemberTagFilter memberTagFilter)
        //{
        //    return MemberSegmentsForMemberTagFilter(memberTagFilter.Id);
        //}
        //public IList<MemberSegment> MemberSegmentsForMemberTagFilter(int memberTagFilterId)
        //{
        //    return MemberSegmentRepository.GetAll().Where(ms => ms.MemberTagFilterId == memberTagFilterId).ToList();
        //}
        public int FilterMemberCount(MemberTagFilter memberTagFilter)
        {
            using (SqlConnection conn = SqlResourceDataAccess.CreateConnection(false, null))
            {
                string countQuery = memberTagFilter.FilterQuery.Replace("*", "COUNT(*)");

                SqlCommand countCommand = conn.CreateCommand();
                countCommand.CommandText = countQuery;

                conn.Open();
                int count = (int)countCommand.ExecuteScalar();
                conn.Close();
                conn.Dispose();

                return count;
            }
        }
        public IList<string> GetUsernameListByMemberSegmentId(int memberSegmentId)
        {
            return MemberSegmentMemberRepository.GetAll().Where(msm => msm.MemberSegmentId == memberSegmentId && msm.StatusType == 1).ToList().Select(msm => MemberService.GetMember(msm.MemberId).Username).ToList();
        }
        public int[] MemberIdsForMemberTagFilter(int memberTagFilterId)
        {
            MemberTagFilter memberTagFilter = TagService.MemberTagFilter(memberTagFilterId);
            return MemberIdsForMemberTagFilter(memberTagFilter);

        }
        public int[] MemberIdsForMemberTagFilter(MemberTagFilter memberTagFilter)
        {
            List<int> memberIds = new List<int>();

            using (SqlConnection conn = SqlResourceDataAccess.CreateConnection(false, null))
            {
                string sqlQuery = memberTagFilter.FilterQuery.Replace("*", "DISTINCT MemberId");

                SqlCommand sqlCommand = conn.CreateCommand();
                sqlCommand.CommandText = sqlQuery;

                conn.Open();

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                while (sqlDataReader.Read())
                {
                    memberIds.Add(Convert.ToInt32(sqlDataReader["MemberId"]));
                }
                sqlDataReader.Close();
                sqlDataReader.Dispose();

                conn.Close();
                conn.Dispose();

                return memberIds.ToArray();
            }
        }
        public bool IsFilterHasMember(int[] memberSegmentIdList, int memberId)
        {
            return MemberSegmentMemberRepository.GetAll().Any(msm => memberSegmentIdList.Contains(msm.MemberSegmentId) && msm.StatusType == 1 && msm.MemberId == memberId);
        }


        public bool IsFilterHasMember(int memberTagFilterId, int memberId)
        {
            MemberTagFilter memberTagFilter = TagService.MemberTagFilter(memberTagFilterId);
            return MemberIdsForMemberTagFilter(memberTagFilter).Any(m => m == memberId);

        }

        public IList<NW.Core.Entities.Member> MembersForMemberTagFilter(int memberTagFilterId)
        {
            MemberTagFilter memberTagFilter = TagService.MemberTagFilter(memberTagFilterId);
            return MembersForMemberTagFilter(memberTagFilter);

        }
        public IList<NW.Core.Entities.Member> MembersForMemberTagFilter(MemberTagFilter memberTagFilter)
        {
            List<NW.Core.Entities.Member> members = new List<NW.Core.Entities.Member>();

            using (SqlConnection conn = SqlResourceDataAccess.CreateConnection(false, null))
            {
                string sqlQuery = memberTagFilter.FilterQuery.Replace("*", "DISTINCT MemberId");

                SqlCommand sqlCommand = conn.CreateCommand();
                sqlCommand.CommandText = sqlQuery;

                conn.Open();

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                while (sqlDataReader.Read())
                {
                    members.Add(MemberService.GetMember(Convert.ToInt32(sqlDataReader["MemberId"])));
                }
                sqlDataReader.Close();
                sqlDataReader.Dispose();

                conn.Close();
                conn.Dispose();

                return members.ToArray();
            }
        }

        public IList<MemberSegment> MemberSegmentListByMemberId(int memberId)
        {
            var member = MemberService.GetMember(memberId);
            var resultModel = HttpHelper.MakeRequestResponseWithHeader("https://pitboss.baymaviservices.com/api/member-segment-maps?filters[MemberUniqueId][$eq]=" + member.UniqueId + "&filters[IsActive][$eq]=true", string.Empty, "GET", "application/json", 500000, new System.Collections.Specialized.NameValueCollection() { { "Authorization", "Bearer 49566791f9bda13927c3a231c500da0db9333a59049fd39771dc649640da6e469b1860db4d65a7ed6bbc7aa1f89ef3c54fe8e4c99a9c2412ab17539175ddb2a0aabb37a19fb113bee4130454d3c5f6d9d2f35ed0e6151d7713dba435a88bf7f7b58587b03aa22c624a6b2732ffc6e4af83479a0e878cc89798eb6b7126a0bd79" } });

            int[] memberSegmentIdList = ((JArray)((JObject)JsonConvert.DeserializeObject(resultModel.Result))["data"]).Select(j => j["SegmentId"].Value<int>()).ToArray();

            return MemberSegmentRepository.GetAll().Where(ms => memberSegmentIdList.Contains(ms.Id) && ms.StatusType == 1).ToList();
        }
        public Dictionary<int, int> CountMemberByMemberSegmentIdList(int[] memberSegmentIdList)
        {
            var resultModel = HttpHelper.MakeRequestResponseWithHeader("https://pitboss.baymaviservices.com/api/member-segment-map/groupByCount?segmentIds=" + string.Join(",", memberSegmentIdList), string.Empty, "GET", "application/json", 500000, new System.Collections.Specialized.NameValueCollection() { { "Authorization", "Bearer 49566791f9bda13927c3a231c500da0db9333a59049fd39771dc649640da6e469b1860db4d65a7ed6bbc7aa1f89ef3c54fe8e4c99a9c2412ab17539175ddb2a0aabb37a19fb113bee4130454d3c5f6d9d2f35ed0e6151d7713dba435a88bf7f7b58587b03aa22c624a6b2732ffc6e4af83479a0e878cc89798eb6b7126a0bd79" } });

            return ((JArray)((JObject)JsonConvert.DeserializeObject(resultModel.Result))["data"]).ToDictionary(j => j["segmentId"].Value<int>(), j => j["total"].Value<int>());
        }


        public DateTime GetLastCronRunDateTime(int memberSegmentId)
        {
            var memberSegmentCronRunHistory = MemberSegmentCronRunHistoryRepository.GetAll().OrderByDescending(msrh => msrh.CreateDate).FirstOrDefault(msrh => msrh.MemberSegmentId == memberSegmentId);
            return memberSegmentCronRunHistory != null ? memberSegmentCronRunHistory.CreateDate : DateTime.MinValue;
        }
        public int InsertMemberSegmentCronRunHistory(int memberSegmentId, int? queryResultCount, int? downgradeMemberCount, int? upgradeMemberCount)
        {
            MemberSegmentCronRunHistory memberSegmentCronRunHistory;
            using (var unitOfWork = UnitOfWork.Current)
            {
                using (ITransaction transaction = unitOfWork.BeginTransaction(Session))
                {
                    memberSegmentCronRunHistory = new MemberSegmentCronRunHistory() { MemberSegmentId = memberSegmentId, DowngradeMemberCount = downgradeMemberCount, UpgradeMemberCount = upgradeMemberCount, QueryResultCount = queryResultCount, CreateDate = DateTime.UtcNow };

                    memberSegmentCronRunHistory = MemberSegmentCronRunHistoryRepository.Insert(memberSegmentCronRunHistory);
                    transaction.Commit();
                }
            }
            return memberSegmentCronRunHistory.Id;
        }
        public void UpdateMemberSegmentCronRunHistory(int id, int memberSegmentId, int? queryResultCount, int? downgradeMemberCount, int? upgradeMemberCount)
        {
            using (var unitOfWork = UnitOfWork.Current)
            {
                using (ITransaction transaction = unitOfWork.BeginTransaction(Session))
                {
                    MemberSegmentCronRunHistory memberSegmentCronRunHistory = MemberSegmentCronRunHistoryRepository.Get(id);

                    memberSegmentCronRunHistory.MemberSegmentId = memberSegmentId;
                    memberSegmentCronRunHistory.QueryResultCount = queryResultCount;
                    memberSegmentCronRunHistory.DowngradeMemberCount = downgradeMemberCount;
                    memberSegmentCronRunHistory.UpgradeMemberCount = upgradeMemberCount;
                    MemberSegmentCronRunHistoryRepository.Update(memberSegmentCronRunHistory);
                    transaction.Commit();
                }
            }
        }
    }
}
