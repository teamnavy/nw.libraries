﻿namespace NW.Service.GameApi.Dto
{
    public class Category
    {
        public int Id { get; set; }
        public int TagValueId { get; set; }
        public int OrderNumber { get; set; }
        public int TemplateId { get; set; }
        public string FriendlyUrl { get; set; }
        public string LogoPath { get; set; }
        public int? ParentId { get; set; }
        public bool Active { get; set; }
        public TagValue TagValue { get; set; }
    }

    public class TagValue
    {
        public int Id { get; set; }
        public string Value { get; set; }
        public Translations Translations { get; set; }
    }
    public class Translations
    {
        public string Tr { get; set; }
        public string En { get; set; }
    }
}
