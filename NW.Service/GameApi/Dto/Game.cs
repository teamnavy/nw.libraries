﻿using NW.Core.Enum;
using System.Collections.Generic;

namespace NW.Service.GameApi.Dto
{
    public class Game
    {
        public int Id { get; set; }
        public string Name { get; set; } = string.Empty;
        public string LogoUrl { get; set; }
        public string ThumbnailUrl { get; set; }
        public string ThumbnailHoverUrl { get; set; }
        public string Alias { get; set; } = string.Empty;
        public string Slug { get; set; } = string.Empty;
        public string ResourceName { get; set; } = string.Empty;
        public string Vendor { get; set; } = string.Empty;
        public int ProviderId { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public string ProviderName { get; set; } = string.Empty;
        public int CasinoGameTypeId { get; set; }
        public int? VoltronGameId { get; set; }
        public bool IsMobile { get; set; }
        public StatusType StatusType { get; set; }
        public Dictionary<string, GameTranslation> Translations { get; set; } = new Dictionary<string, GameTranslation>();
        public List<GameTag> Tags { get; set; } = new List<GameTag>();
        public List<GameCategory> Categories { get; set; } = new List<GameCategory>();
    }
    public class GameTranslation
    {
        public string Name { get; set; } = string.Empty;
        public string Description { get; set; } = string.Empty;
        public string Alias { get; set; } = string.Empty;
    }

    public class GameTag
    {
        public int TagValueId { get; set; }
        public string Value { get; set; } = string.Empty;
        public string Key { get; set; } = string.Empty;
        public Dictionary<string, string> Translations { get; set; } = new Dictionary<string, string>();
    }

    public class GameCategory
    {
        public int CategoryId { get; set; }
        public string FriendlyUrl { get; set; } = string.Empty;
        public int OrderNumber { get; set; }
        public Dictionary<string, string> TagValueTranslations { get; set; } = new Dictionary<string, string>();
    }
}
