﻿using Newtonsoft.Json;
using System.Configuration;
using NW.Helper;
using NW.Service.GameApi.Dto;
using System.Collections.Generic;
using NHibernate.Util;
using System.Web;

namespace NW.Service.GameApi
{
    public class GameApiService
    {
        private readonly string BaseUri;
        private readonly string LastCacheTimestamp;
        public GameApiService()
        {

            BaseUri = ConfigurationManager.AppSettings["GameApiBaseUri"];
            LastCacheTimestamp = ConfigurationManager.AppSettings["LastCacheTimestamp"];
        }
        public Game GetGame(int id, string language)
        {
            // Get categories from GameApi
            if (string.Equals(language, "tr", System.StringComparison.OrdinalIgnoreCase))
            {
                language = "Tr";
            }
            else
            {
                language = "En";
            }
            // Get categories from GameApi
            var headers = new System.Collections.Specialized.NameValueCollection() { { "Authorization", $"Bearer dummie" } };

            var categoryResponse = HttpHelper.MakeRequestResponseWithHeader($"{BaseUri}/Games/{id}?language={language}&_v=" + LastCacheTimestamp, null, "GET", "application/json", 500000, headers);

            ApiResponse<Game> result = JsonConvert.DeserializeObject<ApiResponse<Game>>(categoryResponse.Result);

            if (result?.Data == null)
            {
                return null;
            }

            return result.Data;
        }

        /// <summary>
        /// Get games from GameApi by alias, search, isMobile and language
        /// </summary>
        /// <param name="aliases">Comma separated alias values. If one alias is desired, then pass only one value without any commas.</param>
        /// <param name="search"></param>
        /// <param name="isMobile"></param>
        /// <param name="language"></param>
        /// <returns></returns>
        public List<Game> GetGames(string aliases, string search, bool isMobile, string language)
        {
            // Get categories from GameApi
            if (string.Equals(language, "tr", System.StringComparison.OrdinalIgnoreCase))
            {
                language = "Tr";
            }
            else
            {
                language = "En";
            }
            // Get categories from GameApi
            var headers = new System.Collections.Specialized.NameValueCollection() { { "Authorization", $"Bearer dummie" } };

            string gamesUrl = $"{BaseUri}/Games?language={language}&_v=" + LastCacheTimestamp;

            if (!string.IsNullOrEmpty(search))
            {
                gamesUrl += $"&search={search}";
            }

            if (!string.IsNullOrEmpty(aliases) && aliases.Contains(","))
            {
                gamesUrl += $"&aliases={aliases}";
            }
            else if (!string.IsNullOrEmpty(aliases) && !aliases.Contains(","))
            {
                gamesUrl += $"&alias={aliases}";
            }

            gamesUrl += $"&isMobile={(isMobile ? "true" : "false")}";
            gamesUrl += $"&domain={System.Text.RegularExpressions.Regex.Replace(HttpContext.Current.Request.Url.Host, "\\d", string.Empty).Replace("-uat", string.Empty)}";

            var categoryResponse = HttpHelper.MakeRequestResponseWithHeader(gamesUrl, null, "GET", "application/json", 500000, headers);

            ApiResponse<List<Game>> result = JsonConvert.DeserializeObject<ApiResponse<List<Game>>>(categoryResponse.Result);

            if (result?.Data == null)
            {
                return new List<Game>();
            }

            return result.Data;
        }

        public Category GetCategory(int id, string language)
        {
            // Get categories from GameApi
            if (string.Equals(language, "tr", System.StringComparison.OrdinalIgnoreCase))
            {
                language = "Tr";
            }
            else
            {
                language = "En";
            }
            // Get categories from GameApi
            var headers = new System.Collections.Specialized.NameValueCollection() { { "Authorization", $"Bearer dummie" } };

            var categoryResponse = HttpHelper.MakeRequestResponseWithHeader($"{BaseUri}/Categories?id={id}&language={language}&_v=" + LastCacheTimestamp, null, "GET", "application/json", 500000, headers);

            ApiResponse<Category> result = JsonConvert.DeserializeObject<ApiResponse<Category>>(categoryResponse.Result);

            if (result?.Data == null)
            {
                return null;
            }

            return result.Data;
        }

        public Category GetCategory(string categoryAlias, string language)
        {
            // Get categories from GameApi
            if (string.Equals(language, "tr", System.StringComparison.OrdinalIgnoreCase))
            {
                language = "Tr";
            }
            else
            {
                language = "En";
            }

            var headers = new System.Collections.Specialized.NameValueCollection() { { "Authorization", $"Bearer dummie" } };

            var categoryResponse = HttpHelper.MakeRequestResponseWithHeader($"{BaseUri}/Categories/?friendlyUrl={categoryAlias}&language={language}&_v=" + LastCacheTimestamp, null, "GET", "application/json", 500000, headers);

            ApiResponse<List<Category>> result = JsonConvert.DeserializeObject<ApiResponse<List<Category>>>(categoryResponse.Result);

            if (result?.Data == null || !result.Data.Any())
            {
                return null;
            }

            return result.Data[0];
        }

        public List<Category> GetCategories(string language)
        {
            // Get categories from GameApi
            if (string.Equals(language, "tr", System.StringComparison.OrdinalIgnoreCase))
            {
                language = "Tr";
            }
            else
            {
                language = "En";
            }

            var headers = new System.Collections.Specialized.NameValueCollection() { { "Authorization", $"Bearer dummie" } };

            var categoryResponse = HttpHelper.MakeRequestResponseWithHeader($"{BaseUri}/Categories/?language={language}&_v=" + LastCacheTimestamp, null, "GET", "application/json", 500000, headers);

            ApiResponse<List<Category>> result = JsonConvert.DeserializeObject<ApiResponse<List<Category>>>(categoryResponse.Result);

            if (result?.Data == null)
            {
                return new List<Category>();
            }

            return result.Data;
        }

        //public GameApiSearchResponse<TagValue> GetTopCategories(int templateId)
        //{
        //    // Get categories from GameApi
        //    var headers = new System.Collections.Specialized.NameValueCollection() { { "Authorization", $"Bearer {ConfigurationManager.AppSettings["GameApiAuthToken"]}" } };

        //    var categoryResponse = HttpHelper.MakeRequestResponseWithHeader($"{BaseUri}/tag-values?filters[tag][Name][$eq]=Category&filters[parent][RefId][$eq]={templateId}&sort=OrderNumber", null, "GET", "application/json", headers);

        //    GameApiSearchResponse<TagValue> result = JsonConvert.DeserializeObject<GameApiSearchResponse<TagValue>>(categoryResponse.Result);

        //    return result;
        //}

        //public GameApiSearchResponse<TagValue> GetChildCategories(string documentId)
        //{
        //    // Get cild categories of a category from GameApi
        //    var headers = new System.Collections.Specialized.NameValueCollection() { { "Authorization", $"Bearer {ConfigurationManager.AppSettings["GameApiAuthToken"]}" } };

        //    var categoryResponse = HttpHelper.MakeRequestResponseWithHeader($"{BaseUri}/tag-values?filters[tag][Name][$eq]=Category&filters[parent][documentId][$eq]={documentId}&sort=OrderNumber", null, "GET", "application/json", headers);

        //    GameApiSearchResponse<TagValue> result = JsonConvert.DeserializeObject<GameApiSearchResponse<TagValue>>(categoryResponse.Result);

        //    return result;
        //}

        //public TagValue GetTopCategory(string documentId)
        //{
        //    // Get categories from GameApi
        //    var headers = new System.Collections.Specialized.NameValueCollection() { { "Authorization", $"Bearer {ConfigurationManager.AppSettings["GameApiAuthToken"]}" } };

        //    var categoryResponse = HttpHelper.MakeRequestResponseWithHeader($"{BaseUri}/tag-values/{documentId}?populate=parent", null, "GET", "application/json", headers);

        //    GameApiResponse<TagValue> result = JsonConvert.DeserializeObject<GameApiResponse<TagValue>>(categoryResponse.Result);

        //    string parentCategoryId = result.Data.Parent.DocumentId;

        //    while (!string.IsNullOrEmpty(parentCategoryId))
        //    {
        //        categoryResponse = HttpHelper.MakeRequestResponseWithHeader($"{BaseUri}/tag-values/{parentCategoryId}?populate=parent", null, "GET", "application/json", headers);
        //        result = JsonConvert.DeserializeObject<GameApiResponse<TagValue>>(categoryResponse.Result);
        //        parentCategoryId = result.Data.Parent?.DocumentId;
        //    }

        //    return result.Data;
        //}
    }
}
