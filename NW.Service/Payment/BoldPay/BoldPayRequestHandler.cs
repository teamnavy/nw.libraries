﻿using CloudinaryDotNet;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using NW.Core.Contracts.Payment;
using NW.Core.Entities.Payment;
using NW.Core.Model.Payments;
using NW.Security;
using NW.Services;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NW.Core.Services.Payment;
using System.Web;

namespace NW.Service.Payment.BoldPay
{
    public class BoldPayRequestHandler
    {
        public static GenericPaymentResult SendRequest(object sender, OnePaymentRequestEventArgs e)
        {
            IPaymentService paymentService = (IPaymentService)sender;
            GenericPaymentResult result = new GenericPaymentResult();

            int? companyId = paymentService.CompanyService.CompanyId(e.Domain);
            var appKey = paymentService.CompanyService.GetValue(companyId.Value, "BoldPay.AppKey", e.IsProduction);
            var secret = paymentService.CompanyService.GetValue(companyId.Value, "BoldPay.Secret", e.IsProduction);
            var serviceUrl = paymentService.CompanyService.GetValue(companyId.Value, "BoldPay.ServiceUrl", e.IsProduction);

            string baseUrl = "https://" + e.Domain + "/tr";
            string sUrl = paymentService.CompanyService.GetValue(companyId.Value, "BoldPay.SuccessUrl", e.IsProduction);
            string fUrl = paymentService.CompanyService.GetValue(companyId.Value, "BoldPay.FailUrl", e.IsProduction);


            string data = "amount=" + e.Amount + "&userId=" + e.Member.Id + "&name=" + e.Member.FirstName + "&surname=" + e.Member.LastName + "&username=" + e.Member.Id + "baymavi&processId=" + e.OnePaymentRequest.Id + "&successRedirectUrl=" + baseUrl + sUrl + "&failRedirectUrl=" + baseUrl + fUrl;
            string sign = Convert.ToBase64String(SecurityHelper.GetSignHMACSHA256(secret, data));

            NameValueCollection queryParameters = HttpUtility.ParseQueryString(data);

            // Convert NameValueCollection to Dictionary
            var dict = new System.Collections.Generic.Dictionary<string, string>();
            foreach (string key in queryParameters)
            {
                dict[key] = queryParameters[key];
            }

            string response = Helper.HttpHelper.PostRequest(serviceUrl + "/v1/integration/payco/deposit", JsonConvert.SerializeObject(dict), "application/json", 5000, new NameValueCollection() { { "sign", sign }, { "appKey", appKey } });
            try
            {
                JObject responseObj = JObject.Parse(response);


                if (responseObj["status"].Value<bool>())
                {
                    result.Success = 1;
                    result.RedirectUrl = responseObj["redirect"].Value<string>();
                    result.Data = responseObj;
                    result.UIActionType = NW.Core.Enum.UIActionType.Redirect;
                }
                else
                {
                    result.Data = responseObj["message"].Value<string>();
                }
            }
            catch (Exception ex)
            {
                result.Data = "Bir hata ile karşılaştık. Lütfen daha sonra tekrar deneyin.";
            }



            return result;
        }

    }
}
