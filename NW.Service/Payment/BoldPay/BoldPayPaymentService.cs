﻿using CloudinaryDotNet;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using NW.Core.Contracts.Payment;
using NW.Core.Services.Payment;
using NW.Security;
using NW.Services;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using NW.Core.Entities;
using NW.Core.Services;
using NW.Core.Entities.Payment;

namespace NW.Service.Payment.BoldPay
{
    public class BoldPayPaymentService : IOnePaymentService
    {
        ICompanyService CompanyService { get; set; }
        public BoldPayPaymentService(ICompanyService _companyService)
        {
            CompanyService = _companyService;
        }
        public DepositResult BeforeDeposit()
        {
            return null;
        }

        public GenericPaymentResult StartDeposit(string domain, bool isProduction, long amount, OnePaymentRequest onePaymentRequest, NW.Core.Entities.Member member)
        {
            GenericPaymentResult result = new GenericPaymentResult();

            int? companyId = CompanyService.CompanyId(domain);
            var appKey = CompanyService.GetValue(companyId.Value, "BoldPay.AppKey", isProduction);
            var secret = CompanyService.GetValue(companyId.Value, "BoldPay.Secret", isProduction);
            var serviceUrl = CompanyService.GetValue(companyId.Value, "BoldPay.ServiceUrl", isProduction);

            string baseUrl = "https://" + domain + "/tr";
            string sUrl = CompanyService.GetValue(companyId.Value, "BoldPay.SuccessUrl", isProduction);
            string fUrl = CompanyService.GetValue(companyId.Value, "BoldPay.FailUrl", isProduction);


            string data = "amount=" + amount + "&userId=" + member.Id + "&name=" + member.FirstName + "&surname=" + member.LastName + "&username=" + member.Id + "baymavi&processId=" + onePaymentRequest.Id + "&successRedirectUrl=" + baseUrl + sUrl + "&failRedirectUrl=" + baseUrl + fUrl;
            string sign = Convert.ToBase64String(SecurityHelper.GetSignHMACSHA256(secret, data));

            NameValueCollection queryParameters = HttpUtility.ParseQueryString(data);

            // Convert NameValueCollection to Dictionary
            var dict = new System.Collections.Generic.Dictionary<string, string>();
            foreach (string key in queryParameters)
            {
                dict[key] = queryParameters[key];
            }

            JObject responseObj = JObject.Parse(Helper.HttpHelper.PostRequest(serviceUrl + "/v1/integration/payco/deposit", JsonConvert.SerializeObject(dict), "application/json", 500000, new NameValueCollection() { { "sign", sign }, { "appKey", appKey } }));

            result.Success = 1;
            result.Data = responseObj["redirect"].Value<string>();
            result.UIActionType = NW.Core.Enum.UIActionType.Redirect;
            return result;

        }
    }
}
