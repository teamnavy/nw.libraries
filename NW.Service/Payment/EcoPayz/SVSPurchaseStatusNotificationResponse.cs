﻿using System.Xml.Serialization;

namespace NW.Service.Payment.EcoPayz
{
    [XmlRoot("SVSPurchaseStatusNotificationResponse")]
    public class SVSPurchaseStatusNotificationResponse
    {
        [XmlElement("TransactionResult")]
        public TransactionResult TransactionResult { get; set; }

        [XmlElement("Status")]
        public string Status { get; set; }

        [XmlElement("Authentication")]
        public Authentication Authentication { get; set; }
    }

    public class TransactionResult
    {
        [XmlElement("Description")]
        public string Description { get; set; }

        [XmlElement("Code")]
        public int Code { get; set; }
    }
}
