﻿using CloudinaryDotNet;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using NW.Core.Contracts.Payment;
using NW.Core.Entities.Payment;
using NW.Core.Model.Payments;
using NW.Security;
using NW.Services;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NW.Core.Services.Payment;
using System.Web;

namespace NW.Service.Payment.EcoPayz
{
    public class EcoPayzRequestHandler
    {
        public static GenericPaymentResult SendRequest(object sender, OnePaymentRequestEventArgs e)
        {
            IPaymentService paymentService = (IPaymentService)sender;
            GenericPaymentResult result = new GenericPaymentResult();

            int? companyId = paymentService.CompanyService.CompanyId(e.Domain);
            var paymentPageId = paymentService.CompanyService.GetValue(companyId.Value, "EcoPayz.PaymentPageId", e.IsProduction);
            var accountNumber = paymentService.CompanyService.GetValue(companyId.Value, "EcoPayz.MerchantAccountNumber", e.IsProduction);
            var password = paymentService.CompanyService.GetValue(companyId.Value, "EcoPayz.MerchantPassword", e.IsProduction);
            var serviceUrl = paymentService.CompanyService.GetValue(companyId.Value, "EcoPayz.ServiceUrl", e.IsProduction);

            string baseUrl = "https://" + e.Domain + "/tr";
            string sUrl = paymentService.CompanyService.GetValue(companyId.Value, "EcoPayz.SuccessUrl", e.IsProduction);
            string fUrl = paymentService.CompanyService.GetValue(companyId.Value, "EcoPayz.FailUrl", e.IsProduction);
            string tUrl = paymentService.CompanyService.GetValue(companyId.Value, "EcoPayz.TransferUrl", e.IsProduction);
            string cUrl = paymentService.CompanyService.GetValue(companyId.Value, "EcoPayz.CallbackUrl", e.IsProduction);

            string callbackBaseUrl = "https://www.maviappcontent.com/tr";
            string birthDate = paymentService.MemberService.MemberDetails(e.Member.Id, paymentService.MemberService.RegisterBirthdayKey)[paymentService.MemberService.RegisterBirthdayKey];

            string md5 = SecurityHelper.MD5Encryption(paymentPageId + accountNumber + e.Member.Id + e.OnePaymentRequest.Id + e.Amount + "TRY" + baseUrl + sUrl + baseUrl + fUrl + callbackBaseUrl + tUrl + callbackBaseUrl + cUrl + e.Member.FirstName + e.Member.LastName + birthDate + password);
            string url = $"{serviceUrl}purchase/request?PaymentPageID={paymentPageId}&MerchantAccountNumber={accountNumber}&CustomerIdAtMerchant={e.Member.Id}&TxID={e.OnePaymentRequest.Id}&Amount={e.Amount}&Currency=TRY&OnSuccessUrl={HttpUtility.UrlEncode(baseUrl + sUrl)}&OnFailureUrl={HttpUtility.UrlEncode(baseUrl + fUrl)}&TransferUrl={HttpUtility.UrlEncode(callbackBaseUrl + tUrl)}&CallbackUrl={HttpUtility.UrlEncode(callbackBaseUrl + cUrl)}&FirstName={e.Member.FirstName}&LastName={e.Member.LastName}&DateOfBirth={birthDate}&Checksum={md5}";

            result.Success = 1;
            result.RedirectUrl = url;
            result.UIActionType = NW.Core.Enum.UIActionType.Redirect;
            return result;
        }

    }
}
