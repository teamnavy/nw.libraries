﻿using System;
using System.Xml.Serialization;

namespace NW.Service.Payment.EcoPayz
{
    [XmlRoot("SVSPurchaseStatusNotificationRequest")]
    public class SVSPurchaseStatusNotificationRequest
    {
        [XmlElement("StatusReport")]
        public StatusReport StatusReport { get; set; }

        [XmlElement("Request")]
        public Request Request { get; set; }

        [XmlElement("Authentication")]
        public Authentication Authentication { get; set; }
    }

    public class StatusReport
    {
        [XmlElement("StatusDescription")]
        public string StatusDescription { get; set; }

        [XmlElement("Status")]
        public int Status { get; set; }

        [XmlElement("SVSTransaction")]
        public SVSTransaction SVSTransaction { get; set; }

        [XmlElement("SVSCustomer")]
        public SVSCustomer SVSCustomer { get; set; }
    }

    public class SVSTransaction
    {
        [XmlElement("SVSCustomerAccount")]
        public string SVSCustomerAccount { get; set; }

        [XmlElement("ProcessingTime")]
        public string ProcessingTime { get; set; }

        [XmlElement("Result")]
        public Result Result { get; set; }

        [XmlElement("BatchNumber")]
        public string BatchNumber { get; set; }

        [XmlElement("Id")]
        public string Id { get; set; }
    }

    public class Result
    {
        [XmlElement("Description")]
        public string Description { get; set; }

        [XmlElement("Code")]
        public string Code { get; set; }
    }

    public class SVSCustomer
    {
        [XmlElement("IP")]
        public string IP { get; set; }

        [XmlElement("PostalCode")]
        public string PostalCode { get; set; }

        [XmlElement("Country")]
        public string Country { get; set; }

        [XmlElement("LastName")]
        public string LastName { get; set; }

        [XmlElement("FirstName")]
        public string FirstName { get; set; }
    }

    public class Request
    {
        [XmlElement("MerchantFreeText")]
        public string MerchantFreeText { get; set; }

        [XmlElement("CustomerIdAtMerchant")]
        public string CustomerIdAtMerchant { get; set; }

        [XmlElement("MerchantAccountNumber")]
        public string MerchantAccountNumber { get; set; }

        [XmlElement("Currency")]
        public string Currency { get; set; }

        [XmlElement("Amount")]
        public decimal Amount { get; set; }

        [XmlElement("TxBatchNumber")]
        public int TxBatchNumber { get; set; }

        [XmlElement("TxID")]
        public string TxID { get; set; }

        [XmlElement("UserToken")]
        public Guid UserToken { get; set; }
    }

    public class Authentication
    {
        [XmlElement("Checksum")]
        public string Checksum { get; set; }
    }

}
