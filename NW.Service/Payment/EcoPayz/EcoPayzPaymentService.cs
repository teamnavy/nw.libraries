﻿using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using NW.Core.Contracts.Payment;
using NW.Core.Entities.Payment;
using NW.Core.Services.Payment;
using NW.Core.Services;
using NW.Security;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace NW.Service.Payment.EcoPayz
{
    public class EcoPayzPaymentService : IOnePaymentService
    {
        ICompanyService CompanyService { get; set; }
        public EcoPayzPaymentService(ICompanyService _companyService)
        {
            CompanyService = _companyService;
        }
        public DepositResult BeforeDeposit()
        {
            return null;
        }

        public GenericPaymentResult StartDeposit(string domain, bool isProduction, long amount, OnePaymentRequest onePaymentRequest, NW.Core.Entities.Member member)
        {
            GenericPaymentResult result = new GenericPaymentResult();

            int? companyId = CompanyService.CompanyId(domain);
            var paymentPageId = CompanyService.GetValue(companyId.Value, "EcoPayz.PaymentPageId", isProduction);
            var accountNumber = CompanyService.GetValue(companyId.Value, "EcoPayz.MerchantAccountNumber", isProduction);
            var password = CompanyService.GetValue(companyId.Value, "EcoPayz.MerchantPassword", isProduction);
            var serviceUrl = CompanyService.GetValue(companyId.Value, "EcoPayz.ServiceUrl", isProduction);

            string baseUrl = "https://" + domain + "/tr";
            string sUrl = CompanyService.GetValue(companyId.Value, "EcoPayz.SuccessUrl", isProduction);
            string fUrl = CompanyService.GetValue(companyId.Value, "EcoPayz.FailUrl", isProduction);
            string tUrl = CompanyService.GetValue(companyId.Value, "EcoPayz.TransferUrl", isProduction);
            string cUrl = CompanyService.GetValue(companyId.Value, "EcoPayz.CallbackUrl", isProduction);

            string callbackBaseUrl = "https://www.maviappcontent.com/tr";
            string birthDate = "1980-01-01";

            string md5 = SecurityHelper.MD5Encryption(paymentPageId + accountNumber + member.Id + onePaymentRequest.Id + amount + "TRY" + baseUrl + sUrl + baseUrl + fUrl + callbackBaseUrl + tUrl + callbackBaseUrl + cUrl + member.FirstName + member.LastName + birthDate + password);
            string url = $"{serviceUrl}purchase/request?PaymentPageID={paymentPageId}&MerchantAccountNumber={accountNumber}&CustomerIdAtMerchant={member.Id}&TxID={onePaymentRequest.Id}&Amount={amount}&Currency=TRY&OnSuccessUrl={HttpUtility.UrlEncode(baseUrl + sUrl)}&OnFailureUrl={HttpUtility.UrlEncode(baseUrl + fUrl)}&TransferUrl={HttpUtility.UrlEncode(callbackBaseUrl + tUrl)}&CallbackUrl={HttpUtility.UrlEncode(callbackBaseUrl + cUrl)}&FirstName={member.FirstName}&LastName={member.LastName}&DateOfBirth={birthDate}&Checksum={md5}";

            result.Success = 1;
            result.Data = url;
            result.UIActionType = NW.Core.Enum.UIActionType.Redirect;
            return result;

        }
    }
}
