﻿using System;
using System.Xml.Serialization;

namespace NW.Service.Payment.EcoPayz
{
    [XmlRoot("CallbackStatusNotification")]
    public class CallbackStatusNotification
    {
        [XmlElement("StatusReport")]
        public StatusReport StatusReport { get; set; }

        [XmlElement("Request")]
        public Request Request { get; set; }

        [XmlElement("Authentication")]
        public Authentication Authentication { get; set; }
    }
}
