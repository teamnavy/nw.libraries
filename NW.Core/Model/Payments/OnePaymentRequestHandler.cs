﻿using NW.Core.Contracts.Payment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NW.Core.Model.Payments
{
    public delegate GenericPaymentResult OnePaymentRequestEventHandler(object sender, OnePaymentRequestEventArgs e);
}
