﻿using NW.Core.Entities;
using NW.Core.Entities.Payment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NW.Core.Model.Payments
{
    public class OnePaymentRequestEventArgs
    {
        public bool IsProduction { get; set; }
        public string Domain { get; set; }
        public long Amount { get; set; }
        public Member Member { get; set; }
        public OnePaymentRequest OnePaymentRequest { get; set; }
        public OnePaymentRequestEventArgs(string _domain, bool _isProduction, OnePaymentRequest _onePaymentRequest, Member _member, long amount)
        {
            Domain = _domain;
            IsProduction = _isProduction;
            OnePaymentRequest = _onePaymentRequest;
            Member = _member;
            Amount = amount;

        }

    }
}
