﻿using NW.Core.Contracts.Payment;
using NW.Core.Entities.Payment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NW.Core.Services.Payment
{
    public interface IOnePaymentService
    {
        DepositResult BeforeDeposit();
        GenericPaymentResult StartDeposit(string domain, bool isProduction, long amount, OnePaymentRequest onePaymentRequest, NW.Core.Entities.Member member);
    }
}
