﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NW.Core.Entities.Marketing
{
    public class MarketingCampaign : Entity<int>
    {
        public virtual string Name { get; set; }
        public virtual int StatusType { get; set; }
        public virtual DateTime CreateDate { get; set; }
        public virtual DateTime UpdateDate { get; set; }
        public virtual DateTime? ExpiryDate { get; set; }
        public virtual string AffiliateId { get; set; }
        public virtual int? BonusId { get; set; }
        public virtual int AwardType { get; set; }
        public virtual string Award { get; set; }
        public virtual int CodeCount { get; set; }
        public virtual int CodeUsagePerMember { get; set; }
        public virtual bool IsLastTransactionDeposit { get; set; }


        public virtual IEnumerable<MarketingCampaignCode> MarketingCampaignCodeList { get; set; }

    }
}
