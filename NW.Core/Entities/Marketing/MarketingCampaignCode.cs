﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NW.Core.Entities.Marketing
{
    public class MarketingCampaignCode : Entity<int>
    {
        public virtual string Code { get; set; }
        public virtual int StatusType { get; set; }
        public virtual DateTime CreateDate { get; set; }
        public virtual DateTime UpdateDate { get; set; }
        public virtual int UseCount { get; set; }
        public virtual int UsedCount { get; set; }
        public virtual MarketingCampaign MarketingCampaign { get; set; }
    }
}
