﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NW.Core.Entities
{
    public class CasinoProvider : Entity<int>
    {
        public virtual string Name { get; set; }
        public virtual int StatusType { get; set; }
        public virtual DateTime CreateDate { get; set; }
        public virtual int OrderNumber { get; set; }
        public virtual string LogoPath { get; set; }
        public virtual int VoltronProviderId { get; set; }
        public virtual string SystemName { get; set; }
        public virtual bool IsLive { get; set; }
    }
}
