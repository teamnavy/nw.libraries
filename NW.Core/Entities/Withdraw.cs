﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NW.Core.Entities
{
    public class Withdraw : Transaction
    {
        public string NetworkId { get; set; }
        public int WithdrawStatusType { get; set; }
    }
}
