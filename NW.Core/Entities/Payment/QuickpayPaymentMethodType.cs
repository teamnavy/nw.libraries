﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NW.Core.Entities.Payment
{
    public class QuickpayPaymentMethodType : Entity<int>
    {
        public virtual string Name { get; set; }
        public virtual int VoltronProviderId { get; set; }
    }
}
