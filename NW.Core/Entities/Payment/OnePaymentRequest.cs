﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NW.Core.Entities.Payment
{
    public class OnePaymentRequest : Entity<int>
    {
        public virtual int MemberId { get; set; }
        public virtual int StatusType { get; set; }
        public virtual long Amount { get; set; }
        public virtual DateTime CreateDate { get; set; }
        public virtual DateTime UpdateDate { get; set; }
        public virtual long RecognisedAmount { get; set; }
        public virtual string ProviderUniqueId { get; set; }
        public virtual int PaymentProviderId { get; set; }
        public virtual int PaymentMethodType { get; set; }
        public virtual string RequestData { get; set; }
        public virtual string CallbackData { get; set; }

    }
}
