﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NW.Core.Entities.Payment
{
    public class CryptoAccount : Entity<int>
    {
        public virtual int MemberId { get; set; }
        public virtual string NetworkId { get; set; }
        public virtual int StatusType { get; set; }
        public virtual string Address { get; set; }
        public virtual DateTime CreateDate { get; set; }
    }
}
