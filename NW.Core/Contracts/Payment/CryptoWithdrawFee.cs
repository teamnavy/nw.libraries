﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NW.Core.Contracts.Payment
{
    public class CryptoWithdrawFee
    {
        public int FeeRate { get; set; }
        public decimal Fee { get; set; }
        public string ArrivalEstimation { get; set; }
        public decimal Try { get; set; }
        public decimal Usd { get; set; }
        public decimal Gbp { get; set; }
        public decimal Eur { get; set; }
        public decimal Jpy { get; set; }

    }
}
