﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NW.Core.Contracts.Game
{
    public class JackpotGameResult
    {
        public bool IsSuccess { get; set; }
        public JackpotGame[] JackpotData { get; set; }

    }

    public class JackpotGame
    {
        public int GameId { get; set; }
        public decimal JackpotAmount { get; set; }
    }
}
