﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NW.Core.Enum
{
    public enum PaymentMethodType
    {
        Card,
        BankTransfer,
        Papara,
        Payfix,
        Jeton,
        Cepbank,
        CommunityBankTransfer,
        Moneytolia,
        QR,
        Crytpo,
        Payco,
        Ecopayz
    }
}
