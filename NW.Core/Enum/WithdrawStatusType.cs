﻿namespace NW.Core.Enum
{
    public enum WithdrawStatusType
    {
        WaitingVoltron = -99,
        Rejected = -1,
        Processing = 0,
        Approved = 1,
        Cancelled = 2,
        Sent = 5,
        WaitingProvider = 6
    }
}
